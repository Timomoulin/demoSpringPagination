package com.greta.demopage.dto;

import com.greta.demopage.model.entity.Episode;
import com.greta.demopage.model.entity.Saison;
import com.greta.demopage.model.entity.Serie;
import jakarta.persistence.*;
import org.hibernate.Hibernate;
import org.springframework.web.multipart.MultipartFile;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

public class SaisonDTO {


    private Long id;

    private int num;
    private int annee;


    private Set<Episode> episodes = new LinkedHashSet<>();

    private MultipartFile fichierImage;

    private String cheminImage;

    private Serie serie;

    public Serie getSerie() {
        return serie;
    }

    public void setSerie(Serie serie) {
        this.serie = serie;
    }

    public Set<Episode> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(Set<Episode> episodes) {
        this.episodes = episodes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }



    public MultipartFile getFichierImage() {
        return fichierImage;
    }

    public void setFichierImage(MultipartFile fichierImage) {
        this.fichierImage = fichierImage;
    }


    public String getCheminImage() {
        return cheminImage;
    }

    public void setCheminImage(String cheminImage) {
        this.cheminImage = cheminImage;
    }
}
