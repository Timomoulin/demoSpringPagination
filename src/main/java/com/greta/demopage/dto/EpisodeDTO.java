package com.greta.demopage.dto;

import com.greta.demopage.model.entity.Saison;
import jakarta.persistence.*;
import org.springframework.web.multipart.MultipartFile;

public class EpisodeDTO {

    private Long id;
    private int num;
    private String titre;
    private String realisateur;
    private int duree;


    private String cheminImage;


    private MultipartFile fichierImage;


    private Saison saison;

    public Saison getSaison() {
        return saison;
    }

    public void setSaison(Saison saison) {
        this.saison = saison;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getRealisateur() {
        return realisateur;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getCheminImage() {
        return cheminImage;
    }

    public void setCheminImage(String cheminImage) {
        this.cheminImage = cheminImage;
    }

    public MultipartFile getFichierImage() {
        return fichierImage;
    }

    public void setFichierImage(MultipartFile fichierImage) {
        this.fichierImage = fichierImage;
    }
}
