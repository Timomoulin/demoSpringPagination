package com.greta.demopage.service;

import com.greta.demopage.model.dao.SaisonRepository;
import com.greta.demopage.model.entity.Saison;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SaisonService {
    private SaisonRepository saisonRepository;

    public SaisonService(SaisonRepository saisonRepository) {
        this.saisonRepository = saisonRepository;
    }

    public List<Saison> getAll(){
        return saisonRepository.findAll();
    }
}
