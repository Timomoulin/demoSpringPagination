package com.greta.demopage.service;

import com.greta.demopage.dto.EpisodeDTO;
import com.greta.demopage.model.dao.EpisodeRepository;
import com.greta.demopage.model.entity.Episode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class EpisodeService {
    private EpisodeRepository episodeRepository;
    private  FileUploadService fileUploadService;

    public EpisodeService(EpisodeRepository episodeRepository,FileUploadService fileUploadService) {
        this.episodeRepository = episodeRepository;
        this.fileUploadService= fileUploadService;
    }

    public Page<Episode> getAll(Pageable pageable) {
        return episodeRepository.findAll(pageable);
    }

    public  Page<Episode> getPageEtRecherche(String titreRecheche,Pageable pageable){
        return episodeRepository.findByTitreContainsIgnoreCaseOrderBySaison_Serie_NomAscSaison_NumAsc(titreRecheche,pageable);
    }

    public Episode save(EpisodeDTO episodeDto) throws IOException {
        Episode episode = new Episode();
        if(episodeDto.getId()!=null){
            episodeRepository.findById(episodeDto.getId()).orElseThrow();
        }

        // Créez une instance d'Episode à partir des données de l'EpisodeDTO

        episode.setNum(episodeDto.getNum());
        episode.setTitre(episodeDto.getTitre());
        episode.setRealisateur(episodeDto.getRealisateur());
        episode.setDuree(episodeDto.getDuree());
        episode.setSaison(episodeDto.getSaison());

        // Si une nouvelle image est fournie, téléchargez-la
        if (episodeDto.getFichierImage() != null) {
            String nomFichierImage = fileUploadService.telechargerFichier(episodeDto.getFichierImage(), "episode");

            // Supprimez l'ancienne image si nécessaire
            if (episode.getCheminImage() != null) {
                fileUploadService.supprimerFichier(episode.getCheminImage(),"episode");
            }

            episode.setCheminImage(nomFichierImage);
        }

        // Sauvegardez l'Episode en base de données
        return episodeRepository.save(episode);
    }
}
