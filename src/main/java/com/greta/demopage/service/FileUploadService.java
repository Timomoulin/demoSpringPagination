package com.greta.demopage.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

@Service
public class FileUploadService {

    private final ResourceLoader resourceLoader;

    @Autowired
    public FileUploadService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public String telechargerFichier(MultipartFile fichier, String nomDossier) throws IOException {
        if (fichier.isEmpty()) {
            throw new IllegalArgumentException("Le fichier est vide");
        }

        String nomFichierUnique = UUID.randomUUID().toString() + "_" + fichier.getOriginalFilename();
        String cheminDossier = "src/main/resources/public/img/" + nomDossier + "/";
        String cheminComplet = cheminDossier + nomFichierUnique;

        ClassPathResource resource = new ClassPathResource(cheminComplet);

        File dossier = new File(cheminDossier);
        if (!dossier.exists()) {
            dossier.mkdirs(); // Crée le dossier s'il n'existe pas
        }

        try (FileOutputStream fos = new FileOutputStream(cheminComplet)) {
            fos.write(fichier.getBytes());
        }

        return nomFichierUnique;
    }

    public void supprimerFichier(String nomFichier, String nomDossier) throws IOException {
        String cheminDossier = "src/main/resources/public/img/" + nomDossier + "/";
        String cheminComplet = cheminDossier + nomFichier;
        ClassPathResource resource = new ClassPathResource(cheminComplet);

        if (resource.exists()) {
            resource.getFile().delete();
        }
    }
}
