package com.greta.demopage.controller;

import com.greta.demopage.dto.EpisodeDTO;
import com.greta.demopage.model.entity.Episode;
import com.greta.demopage.service.EpisodeService;
import com.greta.demopage.service.SaisonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Controller
public class EpisodeController {

    private EpisodeService episodeService;

    private SaisonService saisonService;

    @Autowired
    public EpisodeController(EpisodeService episodeService,SaisonService saisonService) {
        this.episodeService = episodeService;
        this.saisonService = saisonService;
    }
//Simple pagination
    @GetMapping("/episode")
    public String getAllEpisodes(Model model, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "") String titre) {
        Pageable pageable = PageRequest.of(page, 6);
        Page<Episode> episodes= episodeService.getPageEtRecherche(titre,pageable);
        model.addAttribute("pageNumber",page);
        model.addAttribute("pageEpisodes",episodes);
        return "episode/indexPagination";
    }

    //Pagination avec trie
    @GetMapping("/episode/order")
    public String getAllEpisodes(Model model,
            @RequestParam(defaultValue = "num") String sortBy,
            @RequestParam(defaultValue = "false") boolean sortDirection,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "4") int pageSize) {

        Sort.Direction direction = sortDirection == true ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageable = PageRequest.of(page, pageSize, Sort.by(direction, sortBy));

        Page<Episode> episodes= episodeService.getAll(pageable);
        model.addAttribute("sortDirection",sortDirection);
        model.addAttribute("pageEpisodes",episodes);
        return "episode/indexOrder";
    }

    @GetMapping("/episode/card")
    public String listerEpisodes(Model model,@RequestParam(defaultValue = "num") String sortBy,
                                 @RequestParam(defaultValue = "false") boolean sortDirection,
                                 @RequestParam(defaultValue = "0") int page,
                                 @RequestParam(defaultValue = "12") int pageSize) {
        // Récupérez la liste des épisodes depuis le service
        Page<Episode> episodes = episodeService.getAll(PageRequest.of(page, pageSize));

        // Ajoutez la liste des épisodes au modèle pour l'affichage
        model.addAttribute("episodes", episodes);

        // Retournez le nom du modèle Thymeleaf à utiliser (par exemple, episodes.html)
        return "episode/indexCard.html";
    }

    @GetMapping("/episode/create")
    public String showAddEpisodeForm(Model model) {
        // Créez un nouvel objet EpisodeDTO et ajoutez-le au modèle pour le formulaire
        EpisodeDTO episodeDto = new EpisodeDTO();
        model.addAttribute("episodeDto", episodeDto);
        //Pour les option du select
        model.addAttribute("saisons",saisonService.getAll());
        return "/episode/create"; // Nom du template HTML
    }

    @PostMapping("/episode")
    public String addEpisode(@ModelAttribute("episodeDto") EpisodeDTO episodeDto) throws IOException {
        episodeService.save(episodeDto);

        return "redirect:/episode/card"; // Redirigez l'utilisateur vers une page de liste d'épisodes
    }
}
